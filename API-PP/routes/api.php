<?php

use App\Http\Controllers\Api\v1\CurrencyController;
use App\Http\Controllers\Api\v1\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(UserController::class)->group(function () {
    Route::post('/login', 'login');
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::controller(CurrencyController::class)->group(function () {
        Route::get('/', 'rates')->name('api.rates');
        Route::post('/', 'convert')->name('api.convert');
    });
});



