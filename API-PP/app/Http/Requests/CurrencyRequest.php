<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'currency_from' => 'required|string',
            'currency_to' => 'required|string',
            'value' => 'required|numeric|min:0.1',
        ];
    }
}
