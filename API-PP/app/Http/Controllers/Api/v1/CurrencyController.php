<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CurrencyRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CurrencyController extends Controller
{

    private function getBCData()
    {
        $response = Http::get('https://blockchain.info/ticker');
        return $response->json();
    }

    private function setComission($block_chain_data)
    {
        foreach ($block_chain_data as $key => $value) {
            $data[$key] = [
                '15m' => $value['15m'] + ($value['15m'] / 100 * 2),
                'last' => $value['last'] + ($value['last'] / 100 * 2),
                'buy' => $value['buy'] + ($value['buy'] / 100 * 2),
                'sell' => $value['sell'],
                'symbol' => $value['symbol'],
            ];
        }

        usort($data, function ($a, $b) {
            return $a['15m'] - $b['15m'];
        });

        return $data;
    }

    public function rates(Request $request)
    {
        if ($request->input('method') == 'rates') {
            $block_chain_data = $this->getBCData();
            $data = $this->setComission($block_chain_data);
            return response()->json(
                [
                    'status' => 'success',
                    'code' => 200,
                    'data' => [
                        $data
                    ]
                ]);
        } else {
            return response()->json(
                [

                    'status' => 'error',
                    'code' => 403,
                    'message' => 'Invalid request method'

                ]);
        }
    }

    public function convert(CurrencyRequest $request)
    {
        $validate = $request->validated();

        if ($request->input('method') == 'convert') {
            $value = $request->input('value');

            $recounted_data = $this->setComission($this->getBCData());
            $data_from = $recounted_data[$validate['currency_from']];
            $data_to = $recounted_data[$validate['currency_to']];

            $raw_converted = $data_from['sell'] / $data_to['buy'] * $value;
            $converted = $raw_converted < 1 ? round($raw_converted, 10) :
                round($raw_converted, 2);

            $data['currency_from'] = $validate['currency_from'];
            $data['currency_to'] = $validate['currency_to'];
            $data['value'] = $value;
            $data['converted_value'] = $converted;
            $data['rate_from'] = $data_from['15m'];
            $data['rate_to'] = $data_to['15m'];
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'data' => [
                    'currency_from' => $data['currency_from'],
                    'currency_to' => $data['currency_to'],
                    'value' => $data['value'],
                    'converted_value' => $data['converted_value'],
                    'rate' => $data['rate_from'] . ' / ' . $data['rate_to']
                ]
            ]);
        } else {
            return response()->json(
                [
                    [
                        'status' => 'error',
                        'code' => 403,
                        'message' => 'Invalid request method'
                    ]
                ]);
        }
    }
}
