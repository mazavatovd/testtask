# API-PP

## Задание 1

WITH 
   tmp AS ( 
      SELECT u.id, u.first_name, u.last_name, b.author, b.name as book FROM users u 
INNER JOIN user_books ub ON u.id = ub.user_id  
INNER JOIN books b on ub.book_id = b.id 
WHERE DATEDIFF(ub.return_date, ub.get_date) < 14 AND 
TIMESTAMPDIFF(YEAR, u.birthday, curdate()) BETWEEN 7 AND 17 AND 
u.id = (SELECT ub.user_id FROM user_books ub 
INNER JOIN books b on ub.book_id = b.id GROUP BY ub.user_id HAVING COUNT(user_id) = 2 AND COUNT(DISTINCT(author)) = 1)) 
   SELECT tmp.id AS ID, CONCAT(tmp.first_name, ' ', tmp.last_name) as Name, tmp.author AS Author, (SELECT tmp.book FROM tmp LIMIT 1) AS Book1, 
   (SELECT tmp.book FROM tmp LIMIT 1 OFFSET 1)  AS Book2 
   FROM tmp limit 1;

## Задание 2

### Инструкция

1) (Только для Windows) Установить и настроить подсистему Linux WSL
2) (Не обязательно) Скачать и установить Docker desktop
3) Настроить окружение, накатить миграции и сиды базы данных (
   php artisan migrate
   php artisan db:seed --class=UserSeeder
   )
4) Запустить контейнер с проектом (vendor/bin/sail up)
5) Открыть коллекцию в Postman (Прилагается), настроить окружение
6) Выполнить авторизацию (запрос 'login'), получить bearer token
7) Передавая в заголовках bearer token протестировать запросы rates и convert.